//
//  SetupWarehouse.swift
//  voice-pick-frontend
//
//  Created by Petter Molnes on 14/04/2023.
//

import SwiftUI

struct SetupWarehouse: View {
	
	@State var joinCodeValue = ""
	@State var warehouseName = ""
	@State var warehouseAddress = ""
	
	@State var showAlert = false
	@State var errorMessage = ""
	
	private let defaultErrorMessage = "Noe gikk galt. Start appen på nytt, eller rapporter en bug."
	
	@EnvironmentObject var authenticationService: AuthenticationService
	
	@ObservedObject var requestService = RequestService()
	
	/**
	 Sets the users warehouse information in the keychain.
	 - Parameters:
	 - warehouse: warehouse information to add to users keychain.
	 */
	func setWarehouseDetails(_ warehouse: WarehouseDto) {
		DispatchQueue.main.async {
			authenticationService.warehouseId = warehouse.id
			authenticationService.warehouseName = warehouse.name
			authenticationService.warehouseAddress = warehouse.address
			
			// Set roles
			authenticationService.roles?.append(RoleDto(id: 2, type: RoleType.LEADER))
		}
	}
	
	/**
	 Sets the refresh token of a user in keychain.
	 */
	func refreshToken() {
		requestService.post(path: "/auth/refresh", body: TokenDto(token: authenticationService.refreshToken), responseType: RefreshTokenResponse.self, completion: { result in
			switch result {
			case .success(let newTokens):
				authenticationService.setTokens(newTokens)
			case .failure(_):
				showAlert = true
				errorMessage = "Noe gikk galt, logg ut og inn."
			}
		})
	}
	
	func createWareHouse() {
		let warehouse = AddWarehouseDto(name: warehouseName, address: warehouseAddress)
		requestService.post(path: "/warehouse", token: authenticationService.accessToken, body: warehouse, responseType: WarehouseDto.self, completion: { result in
			switch result {
			case .success(let warehouse):
				setWarehouseDetails(warehouse)
				print("Warehouse registered")
				refreshToken()
			case .failure(let error as RequestError):
				if error.errorCode == 404 {
					showAlert = true
					errorMessage = "Bruker er ikke autentisert."
				}
			default:
				showAlert = true
				errorMessage = defaultErrorMessage
			}
		})
	}
	
	/**
	 Request warehouse join with verification code.
	 */
	func joinWarehouse() {
		let verificationCodeInfo = VerifyRequestDto(verificationCode: joinCodeValue, email: authenticationService.email)
		requestService.post(path: "/warehouse/join", token: authenticationService.accessToken, body: verificationCodeInfo, responseType: WarehouseDto.self, completion: { result in
			switch result {
			case .success(let warehouse):
				setWarehouseDetails(warehouse)
			case .failure(let error as RequestError):
				handleJoinError(error.errorCode)
			default:
				showAlert = true
				errorMessage = defaultErrorMessage
			}
		})
	}
	
	/**
	 Error handlign for Join request.
	 - Parameters: errorcode
	 */
	func handleJoinError(_ errorCode: Int) {
		switch errorCode {
		case 404:
			errorMessage = "Verifiserings koden er utgått, eller varehuset har blitt slettet."
		case 401:
			errorMessage = "Du er ikke autentisert. Prøv å logg ut og inn."
		default:
			errorMessage = defaultErrorMessage
		}
		showAlert = true
	}
	
	var body: some View {
		ZStack {
			VStack {
				Title("Sett opp varehus")
				Spacer()
				VStack(alignment: .leading) {
					SubTitle("Bli med i varehus")
					DefaultInput(inputLabel: "PIN kode", text: $joinCodeValue, valid: true)
						.padding(.init(.bottom))
					DefaultButton("Bli med", onPress: joinWarehouse)
					HStack {
						VStack {
							Divider()
								.background(Color.foregroundColor)
						}
						Paragraph("eller")
						VStack {
							Divider()
								.background(Color.foregroundColor)
						}
					}
					.padding(.init(top: 10, leading: 0, bottom: 10, trailing: 0))
					SubTitle("Opprett varehus")
					DefaultInput(inputLabel: "Varehus navn", text: $warehouseName, valid: true)
						.padding(.init(top: 0, leading: 0, bottom: 5, trailing: 0))
					DefaultInput(inputLabel: "Varehus addresse", text: $warehouseAddress, valid: true)
						.padding(.bottom)
					DefaultButton("Opprett", onPress: createWareHouse)
					Button("Logg ut", action: {
						authenticationService.logout()
					})
					.frame(maxWidth: .infinity)
					.buttonStyle(.plain)
					.underline()
					.padding()
				}
				Spacer()
			}
			.padding(15)
			.frame(maxWidth: .infinity, maxHeight: .infinity)
			.background(Color.backgroundColor)
			.alert("Error", isPresented: $showAlert, actions: {}, message: { Text("\(errorMessage)") })
			
			if requestService.isLoading {
				ProgressView()
					.progressViewStyle(CircularProgressViewStyle())
					.scaleEffect(2)
					.frame(width: 100, height: 100)
					.background(Color.backgroundColor)
					.cornerRadius(20)
					.foregroundColor(.foregroundColor)
					.padding()
			}
		}
	}
}

struct SetupWarehouse_Previews: PreviewProvider {
	static var previews: some View {
		SetupWarehouse()
	}
}
