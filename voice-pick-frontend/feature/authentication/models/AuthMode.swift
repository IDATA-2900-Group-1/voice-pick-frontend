//
//  AuthMode.swift
//  voice-pick-frontend
//
//  Created by Petter Molnes on 03/03/2023.
//

import Foundation

enum AuthMode {
	case login, signup, verification
}
