//
//  TokenDto.swift
//  voice-pick-frontend
//
//  Created by Joakim Edvardsen on 08/04/2023.
//

import Foundation

struct TokenDto : Codable {
	var token: String
}
