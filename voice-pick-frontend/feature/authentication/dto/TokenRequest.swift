//
//  TokenRequest.swift
//  voice-pick-frontend
//
//  Created by Joakim Edvardsen on 27/03/2023.
//

import Foundation

struct TokenRequest: Codable {
	let token: String
}
