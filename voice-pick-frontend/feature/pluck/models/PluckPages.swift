//
//  PluckPages.swift
//  voice-pick-frontend
//
//  Created by Joakim Edvardsen on 03/03/2023.
//

import Foundation

enum PluckPages {
	case LOBBY
	case INFO
	case LIST_VIEW
	case COMPLETE
	case DELIVERY
}
