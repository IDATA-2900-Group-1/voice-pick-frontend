//
//  AddWarehouseDto.swift
//  voice-pick-frontend
//
//  Created by Petter Molnes on 14/04/2023.
//

import Foundation

struct AddWarehouseDto: Codable {
	let name: String
	let address: String
}
