//
//  WarehouseDto.swift
//  voice-pick-frontend
//
//  Created by Petter Molnes on 19/04/2023.
//

import Foundation

struct WarehouseDto: Codable {
	var id: Int
	var name: String
	var address: String
}
