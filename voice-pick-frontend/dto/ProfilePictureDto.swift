//
//  ProfilePictureDto.swift
//  voice-pick-frontend
//
//  Created by Petter Molnes on 04/05/2023.
//

import Foundation

struct ProfilePictureDto : Codable {
	var pictureName: String
}
